# Robot_snow

Download git repo 
git clone https://gitlab.com/sasidatta/robot_snow.git


Requirements:
Install Python 2/3

Install robot framework python package
pip install robotframework


Choria.robot - Main test cases file
POI_keyword.robot - Keywords consists "model names" for python functions
Functions.py  - Core python file of functions
Variables file - consists of temp variables 

Run Test Suite syntax
python -m robot choria.robot

Run single test case from test suite
python -m robot --test "Sum variables from variables file" choria.robot

output
log & report 
xml file for jenkins attachment