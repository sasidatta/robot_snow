#This file consists to functions only

import pdb
import sys

def sum_two_num(a , b):

    #Python debugger - useful to debug errors at function level
    #pdb.Pdb(stdout=sys.__stdout__).set_trace()
    #Robot framework by default takes input as string
    #convert to Int
    a=int(a)
    b=int(b)
    return (a+b)