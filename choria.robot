*** Settings ***
Documentation    Suite description

#importing Keywords file
Resource          POI_keyword.robot

#Importing Python file as Library

Library            functions.py

*** Variables ***

#Temporary variables
${Root}     400
${Finch}    200

*** Keywords ***

*** Test Cases ***

Perform calculation of Results
    #This prints message to the command window
    log to console  "Performing addition"
    ${result} =  I do add two numbers  ${Root}  ${Finch}
    log to console  ${result}


I do print test message
    log to console  Hello world

I do print test message
    log to console  Hello world

Print variables from variables file
    #Pull variables from variable file
    LOG TO CONSOLE  ${shaw}
    LOG TO CONSOLE  ${fusco}
    ${sum} =  I do add two numbers  ${shaw}  ${fusco}
    log to console  ${sum}